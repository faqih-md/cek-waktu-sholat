# Cek Waktu Sholat

ReactJs App untuk melihat jadwal sholat & imsak berdasarkan geolocation.\
Jadwal sholat berdasarkan KEMENAG RI yang sediakan di [API Fathimah Bot](https://fathimah.docs.apiary.io/).\
Visit published [Apps](https://cek-waktu-sholat.web.app).

# Author
[faqih-md](https://faqih-md.web.app)

# Credits
[Material-Ui](https://github.com/mui-org)\
[banghasan](https://github.com/banghasan)