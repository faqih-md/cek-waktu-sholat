import React, { useState, useEffect } from 'react';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { green } from '@material-ui/core/colors';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import RoomIcon from '@material-ui/icons/Room';
import ExploreIcon from '@material-ui/icons/Explore';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
import 'moment/locale/id';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  appBarIcon: {
    width: 1.3 + "em"
  },
  appMargin: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  tableMargin: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(1)
  },
  selectList: {
    minWidth: 300,
    maxWidth: 300,
    marginLeft: theme.spacing(1)
  },
  table: {
    maxWidth: 30 + "%",
    minWidth: 300
  },
  main: {
    minHeight: "78vh"
  },
  button: {
    margin: theme.spacing(1),
    minWidth: 200
  }
}));

const outerTheme = createMuiTheme({
  palette: {
    primary: {
      main: green[900],
    },
  },
});

function App() {
  const classes = useStyles();

  var initError = {
    isError: false,
    message: ""
  }

  var initData = {
    imsak: "",
    subuh: "",
    dzuhur: "",
    ashar: "",
    maghrib: "",
    isya: ""
  };

  var apiKey = process.env.REACT_APP_TOMTOM_APIKEY;

  const [progress, setProgress] = useState(true);
  const [error, setError] = useState(initError);
  const [hasanCities, setHasanCities] = useState(null);
  const [hasanCity, setHasanCity] = useState(null);
  const [city, setCity] = useState("");
  const [data, setData] = useState(initData);
  const [geoCoords, setGeoCoords] = useState(false);

  const getHasanCities = () => {
    const cities = localStorage.getItem('cities');
    if(cities != null){
      setHasanCities(cities);
    } else {
      fetch("https://api.myquran.com/v2/sholat/kota/semua")
        .then(res => {
          if (res.status === 200) {
            res.json().then(data => {
              localStorage.setItem('cities', JSON.stringify(data));
              setHasanCities(JSON.stringify(data));
            })
          } else {
            handleError(new Error(res.status));
          }
        })
        .catch(error => handleError(error))
    }
  }

  const getHasanData = () => {
    var url = "https://api.myquran.com/v2/sholat/jadwal/"+hasanCity.id+"/"+moment().format('YYYY/MM/DD');
    fetch(url)
      .then(res => {
        if (res.status === 200) {
          res.json().then(obj => {
            setData(obj.data.jadwal)
            setProgress(false);
          })
        } else {
          handleError(new Error(res.status));
        }
      })
      .catch(error => handleError(error))
  }

  const getHasanCity = () => {
    var citiesParse = JSON.parse(hasanCities);
    let citiesFiltered = citiesParse.data.filter(item => item.lokasi.toLowerCase().includes(city.toLowerCase()));
    if(citiesFiltered.length>0){
      let objCity = citiesFiltered[0];
      if(citiesFiltered.length > 1){
        let citiesFiltered2 = citiesFiltered.filter(item => item.lokasi.toLowerCase() === city.toLowerCase());
        if(citiesFiltered2.length > 0){
          objCity = citiesFiltered[0];
        }
      }
      setHasanCity(objCity);
    }
    if(citiesFiltered.length === 0){
      setError({ isError: true, message: "lokasi tidak ditemukan"});
    }
  }

  const getGeocode = (lat, lon) => {
    fetch("https://api.tomtom.com/search/2/reverseGeocode/" + lat + "," + lon + ".json?key="+apiKey)
      .then(res => {
        if (res.status === 200) {
          res.json().then(data => {
            if(data.addresses.length > 0){
              setCity(data.addresses[0].address.localName);
            }
          })
        } else {
          handleError(new Error(res.status));
        }
      })
  }

  const getGeoLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        getGeocode(position.coords.latitude, position.coords.longitude);
      }, error => handleError(error));
  }

  const getIPLocation = () => {
    fetch("https://ipapi.co/json/")
      .then(res => {
        if (res.status === 200) {
          res.json().then(data => {
            setCity(data.city);
          })
        } else {
          handleError(new Error(res.status));
        }
      })
      .catch(error => handleError(error))
  }

  const handleError = (error) => {
    setProgress(false);
    setError({ isError: true, message: error.message });
  }

  const handleButton = () => {
    setGeoCoords(true);
    setProgress(true);
    setError(initError);
    setCity("");
    setHasanCity(null);
    setData(initData);
  }

  const handleQiblaButton = () => {
    window.location.href = "https://qiblafinder.withgoogle.com/";
  }

  useEffect((effect) => {
    if(hasanCities == null && !error.isError){
      getHasanCities();
    }
    if (hasanCities != null && city === "" && !geoCoords && !error.isError) {
      getIPLocation();
    }
    if (hasanCities != null && city === "" && geoCoords && !error.isError) {
      getGeoLocation();
    }
    if(hasanCities != null && city !== "" && hasanCity == null && !error.isError){
      getHasanCity();
    }
    if(hasanCities != null && city !== "" && hasanCity != null && data.subuh === "" && !error.isError){
      getHasanData();
    }
  });

  return (
    <ThemeProvider theme={outerTheme}>
      <AppBar position="static">
        <Toolbar>
          <Button color="inherit" aria-label="menu">
            <Icon className={classes.appBarIcon + " fas fa-mosque"} />
          </Button >
          <Typography variant="h6">Cek Waktu Sholat</Typography>
        </Toolbar>
      </AppBar>
      <Grid container justify="flex-end" className={classes.appMargin}>
        <Typography align="right"><b>{city + (city !== "" ? ", " : "")} {moment().locale('id').format('DD MMMM YYYY')}</b></Typography>
      </Grid>
      <Grid container direction="column" alignItems="center" className={classes.main}>
        <Table className={classes.table + " " + classes.tableMargin}>
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="body2" className="text-color"><b>Imsak</b></Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="body2" className="text-color">{data.imsak}</Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="body2" className="text-color"><b>Shubuh</b></Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="body2" className="text-color">{data.subuh}</Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="body2" className="text-color"><b>Dzuhur</b></Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="body2" className="text-color">{data.dzuhur}</Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="body2" className="text-color"><b>Ashr</b></Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="body2" className="text-color">{data.ashar}</Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="body2" className="text-color"><b>Maghrib</b></Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="body2" className="text-color">{data.maghrib}</Typography>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="body2" className="text-color"><b>Isya</b></Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="body2" className="text-color">{data.isya}</Typography>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
        {progress && <CircularProgress />}
        {!progress &&
          <Button variant="contained"
            color="primary"
            className={classes.button}
            startIcon={<RoomIcon />}
            onClick={handleButton}>
            Update Location
        </Button>
        }
        <Button variant="contained"
            color="primary"
            className={classes.button}
            startIcon={<ExploreIcon />}
            onClick={handleQiblaButton}>
            Qibla Finder
        </Button>
        {error.isError && <Typography color="error" align="center">{error.message}</Typography>}
      </Grid>
      <Grid container direction="column" alignItems="center">
        <Typography><a href="https://faqih-md.web.app/" target="_blank" rel="noopener noreferrer" title="faqih-md" className="footerLink"> ©{moment().format('YYYY')} faqih-md </a></Typography>
      </Grid>
    </ThemeProvider>
  );
}

export default App;
